u"""Testando expressões regulares em um texto."""

import re

caminho = 'texto.txt'

with open(caminho) as arquivo:
    texto = arquivo.read()


def memorial_utm():
    """Quando o memorial apresenta todas os pontos dos vertices em utm."""
    expressao2 = re.compile(r"(?P<nort>(1?[\d])\.((\d{3})\.?){2}(,\d{2}))")
    expressao3 = re.compile(r"((5(\d){2})\.(\d{3}),((\d){2}))")
    expressao4 = re.compile(r"(V\d\d?)")
    nort = [n.group() for n in re.finditer(expressao2, texto)]
    est = [e.group() for e in re.finditer(expressao3, texto)]
    vertice = [v.group() for v in re.finditer(expressao4, texto)]
    geometria = "vertice;x;y\n"
    for v in vertice[:-1]:
        indice = vertice.index(v)
        point = ";".join([v, re.sub(r"\.", "", est[indice]),
                          re.sub(r"\.", "", nort[indice])])
        geometria = geometria + point + "\n"
        salva_arquivo(geometria)


def memorial_rumoaz():
    """Quando o memorial apresenta apenas rumo e azimut"""
    expressao1 = re.compile(r"""(\d?\d)(('|‘|°)(\d{2})){2}("|”)(\w{2})""")
    expressao2 = re.compile(r"""(ponto )(\d?){2}(\d)(.?[A-z])?""")
    expressao3 = re.compile(r"""e (\d?\d?)\d(\.|,)\d\d """)
    grau = [g.group() for g in re.finditer(expressao1, texto)]
    nome_ponto = [v.group() for v in re.finditer(expressao2, texto)]
    nome_ponto = nome_ponto[1:-1]
    dist = [d.group() for d in re.finditer(expressao3, texto)]
    memorial = ''
    print(len(nome_ponto))
    print(nome_ponto)
    print(len(grau))
    print(grau)
    print(len(dist[:-1]))
    #for v in nome_ponto:
    #    indice = nome_ponto.index(v)
    #    point = ";".join([v, grau[indice], dist[indice]])
    #    memorial = point + "\n"



def salva_arquivo(texto, nome_arquivo='coordenadas.txt'):
    """Salva o arquivo TXT."""
    with open(nome_arquivo, 'w') as saida:
        saida.write(texto)
        saida.close()


memorial_rumoaz()
